require 'minitest/autorun'

class Tests < Minitest::Test
end

class Handler
  def initialize(obj)
    @obj = obj.name.gsub(/::/, "_")
  end

  def method(m, &block)
    @method = m
    instance_eval(&block)
  end

  alias :class_method :method

  def test(name = nil, &block)
    name = name.gsub(/\W/, "_") if name
    Tests.define_method("test_#{@obj}_#{@method}_#{name}", &block)
  end
end

class Object
  def assert
    Assertion.new(self)
  end
end

class Assertion
  def initialize(target)
    @target = target
  end

  def ==(other)
    unless @target == other
      raise RuntimeError
    end
  end

end

def testcase(obj, &block)
  handler = Handler.new(obj)
  handler.instance_eval(&block)
end

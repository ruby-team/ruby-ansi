ruby-ansi (1.5.0-2) unstable; urgency=medium

  * Team upload

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Apply multi-arch hints.
    + ruby-ansi: Add :all qualifier for ruby dependency.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

  [ Antonio Terceiro ]
  * Run tests during the build and autopkgtest
  * Drop deprecated *-Ruby-Versions fields
  * Add 2 patches to fix parsing of real life ANSI escape codes
  * Depends: replace `ruby | ruby-interpreter` with ${ruby:Depends}
  * debian/watch: update to use gemwatch
  * debian/rules: install using the Rubygems layout
  * Bump Standards-Version to 4.7.0; no changes needed
  * debian/rules: don't install demo/*

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 24 Oct 2024 13:39:08 -0300

ruby-ansi (1.5.0-1) unstable; urgency=low

  * Team upload
  * New upstream release

 -- Michael Moll <kvedulv@kvedulv.de>  Fri, 14 Aug 2015 00:04:21 +0200

ruby-ansi (1.4.3-1) unstable; urgency=medium

  * Initial release (Closes: #762416)

 -- Tim Potter <tpot@hp.com>  Fri, 19 Sep 2014 11:25:07 +1000

require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['debian/tests']
  t.test_files = FileList['test/case_*.rb'] + ["--exclude=test_ANSI_Code_hex_"]
end
